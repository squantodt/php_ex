<?php
use modal\AddbandRepository;
use modal\GenreRepository;

$genreRepository = new GenreRepository();
$genres = $genreRepository->load();

if (isset($_POST["Add"])) {
    $addbandRepository = new AddbandRepository();
    try {
         mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $addbandRepository->addBand($_POST['Band'], $_POST['Beschrijving'], $_POST['Genre']);
        $band = 1;
    } catch (Exception $e) {
        $band = 2;
    }
    
}
include('view/Addband.html.php');
