<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl ?>recources/styles/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?php echo $baseUrl ?>recources/styles/bootstrap.min.css" >
        <link rel="stylesheet" href="<?php echo $baseUrl ?>recources/styles/modern-business.css">
        
        
        
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"/>
        <title>Bands</title>
    </head>
    
    <body>
        <div class="row">
            <div class="col-xs-12">
                <div class="menubar">
                    <h1 class="titel-pagina">Bands</h1>
                </div>
            </div>
        </div>
        </br>
        <div class="row">
            <div class="col-xs-2">
                <div class="zijbalk">
                    <input class="buttonzijbalk nieuw" type="button" onclick="location.href='<?php echo $baseUrl ?>'" value="Terug" />
                </div>  
            </div>
            <div class="col-xs-1"></div>
            <div class="col-xs-6 kaarten">
                <?php foreach($bands as $band):?>
                    <div class="col-lg-4 col-sm-6 portfolio-item">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">
                                    <p><?php echo $band->getBand(); ?></b>
                                </h4>
                            <p class="card-text"><?php echo $band->getOmschrijving() ?></p>
                            </div>
                        </div>
                    </div>
                <?php  endforeach; ?>
                
            </div>
            <div class="col-xs-3"></div>
        </div>
        <script src="<?php echo $baseUrl ?>recources/js/jquery.min.js"></script>
        <script src="<?php echo $baseUrl ?>recources/js/bootstrap.bundle.min.js"></script>
    </body>
</html>