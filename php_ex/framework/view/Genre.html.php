<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" type="text/css" href="recources/styles/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"/>
        <title>Squanto</title>
    </head>
    
    <body>
        <div class="row">
            <div class="col-xs-12">
                <div class="menubar">
                    <h1 class="titel-pagina">GENRES</h1>
                </div>
            </div>
        </div>
        </br>  <!--LAAT brrr STAAN ANDERS RIP-->
        <div class="row">
            <div class="col-xs-2">
                <div class="zijbalk">
                    <input class="buttonzijbalk nieuw" type="button" onclick="location.href='add'" value="Nieuwe band" />
                </div>  
            </div>
            <div class="col-xs-1"></div>
            <div class="col-xs-6">
                <table class="table table-striped" id="tabelschool">
                    <thead>
                        <tr>
                            <th class="table_title">Genre</th> 
                            <th class="table_title"></th>
                        </tr>
                    </thead>    
                <?php foreach($genres as $genre):?>
                    <tr>
                        <td class="table_td"><?php echo $genre->getGenre()?></td>
                        <div class="back">
                        <td class="table_td">
                            <a href="bands/<?php echo $genre->getId()?>" class="extra" style="cursor: pointer;" title="Meer info" data-toggle="tooltip" Name="Extra"><i class="material-icons">&#x2b;</i></a>
                        </td>
                        </div>            
                    </tr>
                <?php endforeach; ?>
                </table>
            </div>
            <div class="col-xs-3"></div>
        </div>
        
                
        
        
    </body>
    
<script>
    $(document).ready(function() {
    $('#tabelschool').DataTable({
        "language":{
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json"
        }
    });
});
</script>
</html>