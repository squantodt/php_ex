<html>
    <head>
        <link rel="stylesheet" type="text/css" href="recources/styles/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>Voeg toe</title>
    </head>
    
    <body>
        <div class="row">
            <div class="col-xs-12">
                <div>
                    <div class="menubar">
                    <h1 class="titel-pagina">VOEG EEN BAND TOE</h1>
                    </div>
                </div>
            </div>
        </div>
        </br>  <!--LAAT brrr STAAN ANDERS RIP-->
        <div class="row">
            <div class="col-xs-2">
                <div class="zijbalk">
                    <input class="buttonzijbalk nieuw" type="button" onclick="location.href='<?php echo $baseUrl ?>'" value="Terug" />
                </div> 
            </div>
            <div class="col-xs-0"></div>
            
            <div class="col-xs-6">
                
                <form class="scholen_form" method="POST">
                    <div class="ding">
                        <?php if ($band == 1) { ?>
                            <div class="alert alert-warning" role="alert">
                                <p>Deze band zit al in de database</p> 
                            </div>
                        <?php }
                        else if ($band == 2) {
                        ?>
                            <div class="alert alert-success" role="alert">
                                <p>Deze band is successvol toegevoegd aan de database </p>
                            </div>
                        <?php  
                        }
                        $band = 0; ?>
                    </div>
                    <div class="form-group">
                        <label for="toestelnaam">Band:</label>
                        <input type="text" class="form-control" name="Band"  placeholder="Naam band" required>
                    </div>
                    <div class="form-group">
                        <label for="toestelnaam">Beschrijving:</label>
                        <input type="text" class="form-control" name="Beschrijving"  placeholder="Naam band" required>
                    </div>
                    <div class="form-group">
                        <label for="toestelschool">Genre</label>
                        <select class="form-control" name="Genre">
                          <?php foreach($genres as $genre) : ?>
                              <option value="<?php echo $genre->getId()?>"><?php echo $genre->getGenre()?></option>
                          <?php endforeach; ?>
                        </select>
                    </div>
                    </br>
                    <input class="button" type="submit" name="Add" value="Voeg toe"></input>
                </form>
            </div>
            <div class="col-xs-4"></div>
        </div>
        <script>
            if (document.getElementById("modalopen")) {
                $("#myModal").modal();
            }
        </script>
    </body>
</html>