<?php
namespace modal;
class BandRepository{
    
    const COL_IDBAND = "Idband";
    const COL_NAAM = "Naam";
    const COL_OMSCHRIJVING = "Omschrijving";
    
    public function findArtiesten($id){
        
        global $dbConn;
        
        $columns = array(
            self::COL_IDBAND,
            self::COL_NAAM,
            self::COL_OMSCHRIJVING
            );
            
        $sql = 'SELECT '.implode(', ', $columns).' FROM Bands WHERE Genre = ?';
        $stmt = $dbConn->prepare($sql);
        $stmt->bind_param('d', $id);
        $stmt->execute();
        
        $bands = array();
        foreach ($stmt->get_result() as $item) {
            $band = new Band();
            $band->setBand($item[self::COL_NAAM]);
            $band->setOmschrijving($item[self::COL_OMSCHRIJVING]);
            $bands[] = $band;
        }
        $stmt->close();
        return $bands;
    }
}
