<?php

namespace modal;

class Band {
    private $omschrijving;
    private $band;
    
    public function getOmschrijving(){
		return $this->omschrijving;
	}

	public function setOmschrijving($omschrijving){
		$this->omschrijving = $omschrijving;
	}

	public function getBand(){
		return $this->band;
	}

	public function setBand($band){
		$this->band = $band;
	}
}