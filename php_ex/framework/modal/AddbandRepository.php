<?php
namespace modal;
class AddbandRepository{
    
    const COL_IDBAND = "Idband";
    const COL_NAAM = "Naam";
    const COL_OMSCHIJVING = "Omschrijving";
    const COL_GENRE = "Genre";
    
    
    public function addBand($band, $omschrijving, $genre){
        
        global $dbConn;
        
        $columns = array(
            self::COL_NAAM,
            self::COL_OMSCHIJVING,
            self::COL_GENRE
            );
        
        $values = array(
            $band,
            $omschrijving,
            $genre
            );
            
        $sql = "INSERT INTO Bands (".implode(', ', $columns).") VALUES ('".implode("', '", $values)."')";
        
        $dbConn->query($sql);
        $result = true;
        return $result;
    }
}