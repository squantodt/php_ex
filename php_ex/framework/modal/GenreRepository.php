<?php

namespace modal;

class GenreRepository {
    const COL_ID = "Idgenre";
    const COL_GENRE = "Genre";
    
    
    public function load(){
        
        global $dbConn;
        
        $columns = array(
            self::COL_ID,
            self::COL_GENRE
            );
        
        $sql = 'SELECT '.implode(', ', $columns).' FROM Genres';
        $result = $dbConn->query($sql);
        $genres = array();
        foreach ($result as $item) {
            $genre = new Genre();
            $genre->setId($item[self::COL_ID]);
            $genre->setGenre($item[self::COL_GENRE]);
            
            $genres[] = $genre;
        }
        return $genres;
        //return $result;
    }
}