<?php

include('include/autoloader.php');
include('include/parameters.php');
session_start();

//database connectie
$dbConn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);
if ($dbConn->connect_error) {
	die('Failed to connect to MySQL - ' . $dbConn->connect_error);
}

$uri = rtrim( dirname($_SERVER["SCRIPT_NAME"]), '/' );
$uri = '/' . trim( str_replace( $uri, '', $_SERVER['REQUEST_URI'] ), '/' );
$uri = urldecode( $uri );

$baseUrl = str_replace('index.php','',$_SERVER["SCRIPT_NAME"]);
//$baseUrl = $_SERVER["SCRIPT_NAME"];

$routes = array(
    'Genre'            => "/",                           // '/'
    'Bands'            => "/bands/(?'id'\d+)",            // bands
    'Addband'          => "/add"
);

foreach ($routes as $action => $route)
{
    if ( preg_match( '~^'.$route.'$~i', $uri, $routeParameters ) )
    {
        cleanup($routeParameters);
        include( 'controller/'.$action.'.php' );
        exit();
    }
}

include( 'view/404.html.php' );


function cleanup($array) {
    foreach ($array as $key=>$value){
        if(gettype($key)=='integer'){
            unset($array[$key]);
        }
    }
}